package dartlexx.exchanger.di.exchange;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.bussiness.ExchangeRatesInteractor;
import dartlexx.exchanger.bussiness.IExchangeRatesInteractor;
import dartlexx.exchanger.bussiness.calculations.ExchangeCalculator;
import dartlexx.exchanger.data.network.ConnectionAvailableReceiver;
import dartlexx.exchanger.data.network.FixerIoRatesRequest;
import dartlexx.exchanger.data.network.ICurrencyRatesRequest;
import dartlexx.exchanger.data.network.IFixerIoExchangeRates;
import dartlexx.exchanger.data.repository.CurrencyRatesRepository;
import dartlexx.exchanger.data.repository.ICurrencyRatesRepository;
import dartlexx.exchanger.ui.presenter.ExchangerPresenter;
import dartlexx.exchanger.ui.presenter.IExchangerPresenter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ExchangeModule {

    private final Context mAppContext;

    public ExchangeModule(Context appContext) {
        mAppContext = appContext;
    }

    @Provides
    @ExchangeScope
    Retrofit provideFixerIoRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(FixerIoRatesRequest.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @ExchangeScope
    ICurrencyRatesRequest provideCurrencyRatesRequest(Retrofit retrofit) {
        return new FixerIoRatesRequest(retrofit.create(IFixerIoExchangeRates.class),
                ExchangerApp.DEFAULT_BASE_CURRENCY,
                ExchangerApp.DEFAULT_EXCHANGE_RATES);
    }

    @Provides
    @ExchangeScope
    ICurrencyRatesRepository provideCurrencyRatesRepository(ICurrencyRatesRequest ratesRequest) {
        return new CurrencyRatesRepository(ratesRequest);
    }

    @Provides
    @ExchangeScope
    ExchangeCalculator provideExchangeCalculator() {
        return new ExchangeCalculator();
    }

    @Provides
    @ExchangeScope
    ConnectionAvailableReceiver provideConnectionAvailableReceiver() {
        return new ConnectionAvailableReceiver(mAppContext);
    }

    @Provides
    @ExchangeScope
    IExchangeRatesInteractor provideExchangeRatesInteractor(ICurrencyRatesRepository repository,
                                                            ExchangeCalculator calculator,
                                                            ConnectionAvailableReceiver receiver) {
        return new ExchangeRatesInteractor(repository, calculator, receiver);
    }

    @Provides
    @ExchangeScope
    IExchangerPresenter provideExchangerPresenter(IExchangeRatesInteractor interactor) {
        return new ExchangerPresenter(interactor, ExchangerApp.SUPPORTED_CURRENCIES);
    }
}
