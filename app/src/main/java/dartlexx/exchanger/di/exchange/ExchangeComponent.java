package dartlexx.exchanger.di.exchange;

import dagger.Subcomponent;
import dartlexx.exchanger.ui.views.ExchangerFragment;

@Subcomponent(modules = ExchangeModule.class)
@ExchangeScope
public interface ExchangeComponent {

    void inject(ExchangerFragment o);
}