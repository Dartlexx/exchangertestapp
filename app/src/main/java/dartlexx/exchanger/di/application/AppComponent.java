package dartlexx.exchanger.di.application;

import javax.inject.Singleton;

import dagger.Component;
import dartlexx.exchanger.di.exchange.ExchangeComponent;
import dartlexx.exchanger.di.exchange.ExchangeModule;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {

    ExchangeComponent addComponent(ExchangeModule module);
}
