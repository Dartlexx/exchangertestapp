package dartlexx.exchanger.data.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

import rx.Observable;
import rx.subjects.BehaviorSubject;

public class ConnectionAvailableReceiver extends BroadcastReceiver {

    private final Context mContext;
    private final BehaviorSubject<Boolean> mSubject;

    public ConnectionAvailableReceiver(@NonNull Context context) {
        mContext = context;
        mSubject = BehaviorSubject.create();
    }

    public Observable<Boolean> registerReceiverAndSubscribe() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.registerReceiver(this, filter);
        return mSubject.asObservable();
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
            boolean hasConnection = !intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            if (hasConnection) {
                mSubject.onNext(true);
            }
        }
    }
}
