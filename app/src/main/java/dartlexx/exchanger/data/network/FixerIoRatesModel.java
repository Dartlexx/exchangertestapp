package dartlexx.exchanger.data.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Map;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.data.repository.CurrencyRatesModel;
import dartlexx.exchanger.data.repository.CurrencyType;
import dartlexx.exchanger.data.repository.ModelMappingException;

public class FixerIoRatesModel {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-mm-dd", Locale.US);

    @Nullable
    private String base;
    @Nullable
    private String date;
    @Nullable
    private Map<String, String> rates;

    @VisibleForTesting
    FixerIoRatesModel(@Nullable String baseCurrency,
                      @Nullable String updateDate,
                      @Nullable Map<String, String> exchangeRates) {
        base = baseCurrency;
        date = updateDate;
        rates = exchangeRates;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("Rates for ");
        builder
                .append('\'')
                .append(base)
                .append("\'")
                .append("\n\tDate: ")
                .append(date);
        if (rates != null) {
            for (Map.Entry<String, String> rate : rates.entrySet()) {
                builder
                        .append("\n\t\t")
                        .append(rate.getKey())
                        .append(": ")
                        .append(rate.getValue());
            }
        }
        return builder.toString();
    }

    @NonNull
    public CurrencyRatesModel convertToCurrencyRates() {
        if (TextUtils.isEmpty(base) ||
                TextUtils.isEmpty(date) ||
                rates == null ||
                rates.isEmpty()) {
            throw new ModelMappingException("Failed to convert 'FixerIoRatesModel' to " +
                    "'CurrencyRatesModel': illegal arguments");
        }

        try {
            final CurrencyType baseCurrency = CurrencyType.valueOf(base);
            final Date updatedDate = DATE_FORMAT.parse(date);

            final Map<CurrencyType, BigDecimal> currencyRates = new EnumMap<>(CurrencyType.class);
            for (Map.Entry<String, String> rate : rates.entrySet() ){
                currencyRates.put(CurrencyType.valueOf(rate.getKey()),
                        new BigDecimal(rate.getValue(), ExchangerApp.MATH_CONTEXT));
            }
            return new CurrencyRatesModel(baseCurrency, updatedDate, currencyRates);
        } catch (IllegalArgumentException | ParseException exc) {
            throw new ModelMappingException("Failed to convert 'FixerIoRatesModel' to " +
                    "'CurrencyRatesModel'", exc);
        }
    }
}