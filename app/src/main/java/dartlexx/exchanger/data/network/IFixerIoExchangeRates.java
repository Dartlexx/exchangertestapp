package dartlexx.exchanger.data.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IFixerIoExchangeRates {

    @GET("/latest")
    Call<FixerIoRatesModel> ratesForBaseCurrency(@Query("base") String baseCurrency,
                                                 @Query("symbols") String currencies);
}
