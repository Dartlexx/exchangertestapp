package dartlexx.exchanger.data.network;

import android.support.annotation.NonNull;

import rx.Single;

public interface ICurrencyRatesRequest {

    @NonNull
    Single<FixerIoRatesModel> getRates();

    @NonNull
    Single<FixerIoRatesModel> getRates(String baseCurrency, String exchangeRatesString);
}
