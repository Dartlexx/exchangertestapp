package dartlexx.exchanger.data.network;

import android.support.annotation.NonNull;
import android.util.Log;

import dartlexx.exchanger.data.repository.ModelMappingException;
import retrofit2.Call;
import retrofit2.Response;
import rx.Single;

public class FixerIoRatesRequest implements ICurrencyRatesRequest {

    public static final String SERVER_URL = "http://api.fixer.io";

    private static final String TAG = FixerIoRatesRequest.class.getSimpleName();

    private final IFixerIoExchangeRates mExchangeRatesGetter;
    private final String mDefaultBaseCurrency;
    private final String mDefaultExchangeCurrencies;
    private Call<FixerIoRatesModel> mDefaultGetRatesCall;

    public FixerIoRatesRequest(@NonNull IFixerIoExchangeRates exchangeRatesInterface,
                               @NonNull String baseCurrency,
                               @NonNull String exchangeCurrencies) {
        mExchangeRatesGetter = exchangeRatesInterface;
        mDefaultBaseCurrency = baseCurrency;
        mDefaultExchangeCurrencies = exchangeCurrencies;
    }

    @NonNull
    @Override
    public Single<FixerIoRatesModel> getRates() {
        if (mDefaultGetRatesCall != null) {
            return executeCall(mDefaultGetRatesCall.clone());
        } else {
            mDefaultGetRatesCall = mExchangeRatesGetter.ratesForBaseCurrency(mDefaultBaseCurrency,
                    mDefaultExchangeCurrencies);
            return executeCall(mDefaultGetRatesCall);
        }
    }

    @NonNull
    @Override
    public Single<FixerIoRatesModel> getRates(String baseCurrency, String exchangeRatesString) {
        final Call<FixerIoRatesModel> request = mExchangeRatesGetter.ratesForBaseCurrency(
                baseCurrency,
                exchangeRatesString);
        return executeCall(request);
    }

    private Single<FixerIoRatesModel> executeCall(@NonNull Call<FixerIoRatesModel> call) {
        return Single
                .fromCallable(call::execute)
                .map(this::mapResponseToModel)
                .doOnSuccess(fixerRate -> Log.d(TAG, "Received " + fixerRate))
                .doOnError(error -> Log.e(TAG, "Rates request failed", error));
    }

    private FixerIoRatesModel mapResponseToModel(Response<FixerIoRatesModel> response) {
        if (!response.isSuccessful()) {
            throw new ModelMappingException("Request failed: errorCode=" + response.code());
        }
        if (response.body() == null) {
            throw new ModelMappingException("Request failed: response body is NULL");
        }
        return response.body();
    }
}
