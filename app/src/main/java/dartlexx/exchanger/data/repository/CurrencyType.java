package dartlexx.exchanger.data.repository;

import android.support.annotation.NonNull;

public enum CurrencyType {

    EUR,
    GBP,
    USD;

    public static char getCurrencySymbol(@NonNull CurrencyType type) {
        switch (type) {
            case EUR:
                return '€';
            case GBP:
                return '£';
            case USD:
                return '$';
            default:
                throw new IllegalArgumentException("Unknown CurrencyType = " + type);
        }
    }
}