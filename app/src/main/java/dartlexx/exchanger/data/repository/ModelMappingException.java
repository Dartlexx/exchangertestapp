package dartlexx.exchanger.data.repository;

public class ModelMappingException extends RuntimeException {

    public ModelMappingException(String message) {
        super(message);
    }

    public ModelMappingException(String message, Throwable error) {
        super(message, error);
    }
}
