package dartlexx.exchanger.data.repository;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import dartlexx.exchanger.data.network.FixerIoRatesModel;
import dartlexx.exchanger.data.network.ICurrencyRatesRequest;
import rx.Single;

public class CurrencyRatesRepository implements ICurrencyRatesRepository {

    private final ICurrencyRatesRequest mExchangeRatesRequest;

    public CurrencyRatesRepository(@NonNull ICurrencyRatesRequest exchangeRatesRequest) {
        mExchangeRatesRequest = exchangeRatesRequest;
    }

    @Override
    public Single<CurrencyRatesModel> getDefaultCurrencyRates() {
        return mExchangeRatesRequest
                .getRates()
                .map(FixerIoRatesModel::convertToCurrencyRates);
    }

    @Override
    public Single<CurrencyRatesModel> getCurrencyRatesFor(@NonNull CurrencyType baseCurrency,
                                                          @NonNull CurrencyType[] exchangeRates) {
        final String currencies = buildCurrenciesEnumeration(exchangeRates);
        return mExchangeRatesRequest.getRates(baseCurrency.toString(), currencies)
                .map(FixerIoRatesModel::convertToCurrencyRates);
    }

    @VisibleForTesting
    static String buildCurrenciesEnumeration(@NonNull CurrencyType[] exchangeRates) {
        final StringBuilder builder = new StringBuilder();
        for (int i=0; i < exchangeRates.length; i++) {
            if (i != 0) {
                builder.append(",");
            }
            builder.append(exchangeRates[i]);
        }
        return builder.toString();
    }
}
