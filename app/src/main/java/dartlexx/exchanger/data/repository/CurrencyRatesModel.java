package dartlexx.exchanger.data.repository;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;

public class CurrencyRatesModel {

    private CurrencyType mBaseCurrency;
    private Date mLastUpdatedAt;
    private Map<CurrencyType, BigDecimal> mRates = new EnumMap<>(CurrencyType.class);

    public CurrencyRatesModel(@NonNull CurrencyType baseCurrency,
                              @NonNull Date updatedAt,
                              @NonNull Map<CurrencyType, BigDecimal> rates) {
        mBaseCurrency = baseCurrency;
        mLastUpdatedAt = updatedAt;
        mRates.putAll(rates);
    }

    @NonNull
    public CurrencyType getBaseCurrency() {
        return mBaseCurrency;
    }

    @NonNull
    public Date getLastUpdatedAt() {
        return mLastUpdatedAt;
    }

    @NonNull
    public Map<CurrencyType, BigDecimal> getCurrencyRates() {
        return mRates;
    }
}
