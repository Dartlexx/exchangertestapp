package dartlexx.exchanger.data.repository;

import android.support.annotation.NonNull;

import rx.Single;

public interface ICurrencyRatesRepository {

    Single<CurrencyRatesModel> getDefaultCurrencyRates();

    @SuppressWarnings("unused")
    Single<CurrencyRatesModel> getCurrencyRatesFor(@NonNull CurrencyType baseCurrency,
                                                   @NonNull CurrencyType[] exchangeRates);
}