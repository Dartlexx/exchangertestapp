package dartlexx.exchanger;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import dartlexx.exchanger.ui.views.ExchangerFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_EXCHANGER_FRAGMENT = ExchangerFragment.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchanger);

        if (getSupportFragmentManager().findFragmentByTag(TAG_EXCHANGER_FRAGMENT) == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(new ExchangerFragment(), TAG_EXCHANGER_FRAGMENT)
                    .commit();
        }
    }
}
