package dartlexx.exchanger;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import java.math.MathContext;
import java.math.RoundingMode;

import dartlexx.exchanger.data.repository.CurrencyType;
import dartlexx.exchanger.di.application.AppComponent;
import dartlexx.exchanger.di.application.DaggerAppComponent;

public class ExchangerApp extends Application {

    // App constants
    public static final MathContext MATH_CONTEXT = new MathContext(8, RoundingMode.HALF_UP);
    public static final String DEFAULT_EXCHANGE_RATES = "USD,GBP";
    public static final String DEFAULT_BASE_CURRENCY = "EUR";
    public static final CurrencyType[] SUPPORTED_CURRENCIES = new CurrencyType[] {
            CurrencyType.EUR,
            CurrencyType.USD,
            CurrencyType.GBP
    };

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder().build();
    }

    public static AppComponent getAppComponent(@NonNull Context context) {
        return ((ExchangerApp) context.getApplicationContext()).mAppComponent;
    }
}
