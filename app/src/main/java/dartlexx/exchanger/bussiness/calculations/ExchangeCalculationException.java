package dartlexx.exchanger.bussiness.calculations;

class ExchangeCalculationException extends RuntimeException {

    ExchangeCalculationException(String message) {
        super(message);
    }
}
