package dartlexx.exchanger.bussiness.calculations;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.math.BigDecimal;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.bussiness.models.CurrencyExchangeResultModel;
import dartlexx.exchanger.data.repository.CurrencyRatesModel;
import dartlexx.exchanger.data.repository.CurrencyType;
import dartlexx.exchanger.ui.models.CurrencyExchangeParamsModel;

public class ExchangeCalculator {

    @NonNull
    public CurrencyExchangeResultModel calcExchangedAmount(@NonNull CurrencyExchangeParamsModel exchangeParams,
                                                           @NonNull CurrencyRatesModel rates) {
        BigDecimal exchangeRate = getRateForExchange(exchangeParams.getFromCurrency(),
                exchangeParams.getTargetCurrency(), rates);
        BigDecimal targetAmount = exchangeParams.getAmount().multiply(exchangeRate,
                ExchangerApp.MATH_CONTEXT);
        return new CurrencyExchangeResultModel(exchangeRate, targetAmount);
    }

    @VisibleForTesting
    @NonNull
    BigDecimal getRateForExchange(@NonNull CurrencyType fromCurrency,
                                  @NonNull CurrencyType targetCurrency,
                                  @NonNull CurrencyRatesModel rates) {
        if (fromCurrency.equals(targetCurrency)) {
            return BigDecimal.ONE;
        }

        if (fromCurrency.equals(rates.getBaseCurrency())) {
            return findRateForBaseCurrency(targetCurrency, rates);
        }

        if (targetCurrency.equals(rates.getBaseCurrency())) {
            return BigDecimal
                    .ONE
                    .divide(findRateForBaseCurrency(fromCurrency, rates), ExchangerApp.MATH_CONTEXT);
        }

        BigDecimal rateBaseToTarget = findRateForBaseCurrency(targetCurrency, rates);
        BigDecimal rateBaseToFrom = findRateForBaseCurrency(fromCurrency, rates);
        return rateBaseToTarget.divide(rateBaseToFrom, ExchangerApp.MATH_CONTEXT);
    }

    @VisibleForTesting
    @NonNull
    BigDecimal findRateForBaseCurrency(@NonNull CurrencyType targetCurrency,
                                       @NonNull CurrencyRatesModel rates) {
        BigDecimal rate = rates.getCurrencyRates().get(targetCurrency);
        if (rate == null) {
            throw new ExchangeCalculationException("Not found exchange rate for '" +
                    targetCurrency + "' currency");
        }
        if (rate.signum() != 1) {
            throw new ExchangeCalculationException("Exchange rate for '" +
                    targetCurrency + "' currency is INCORRECT = " + rate);
        }
        return rate;
    }
}
