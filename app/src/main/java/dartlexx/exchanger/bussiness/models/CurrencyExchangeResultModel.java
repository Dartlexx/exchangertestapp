package dartlexx.exchanger.bussiness.models;

import android.support.annotation.NonNull;

import java.math.BigDecimal;

public class CurrencyExchangeResultModel {

    private final BigDecimal mExchangeRate;
    private final BigDecimal mTargetAmount;

    public CurrencyExchangeResultModel(@NonNull BigDecimal exchangeRate,
                                       @NonNull BigDecimal targetAmount) {
        mExchangeRate = exchangeRate;
        mTargetAmount = targetAmount;
    }

    @NonNull
    public BigDecimal getExchangeRate() {
        return mExchangeRate;
    }

    @NonNull
    public BigDecimal getTargetAmount() {
        return mTargetAmount;
    }

    @Override
    public String toString() {
        return "CurrencyExchangeResultModel{" +
                "exchangeRate=" + mExchangeRate +
                "; targetAmount=" + mTargetAmount +
                '}';
    }
}
