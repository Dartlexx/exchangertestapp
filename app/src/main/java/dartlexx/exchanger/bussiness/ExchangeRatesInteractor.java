package dartlexx.exchanger.bussiness;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import dartlexx.exchanger.bussiness.calculations.ExchangeCalculator;
import dartlexx.exchanger.bussiness.models.CurrencyExchangeResultModel;
import dartlexx.exchanger.data.network.ConnectionAvailableReceiver;
import dartlexx.exchanger.data.repository.CurrencyRatesModel;
import dartlexx.exchanger.data.repository.ICurrencyRatesRepository;
import dartlexx.exchanger.ui.models.CurrencyExchangeParamsModel;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class ExchangeRatesInteractor implements IExchangeRatesInteractor {

    private static final int DELAY_SECONDS = 30;

    private final ICurrencyRatesRepository mRatesRepository;
    private final ExchangeCalculator mCalculator;
    private final ConnectionAvailableReceiver mConnAvailableReceiver;

    private CurrencyExchangeParamsModel mExchangeParams;
    private CurrencyRatesModel mCachedCurrencyRates;
    private BehaviorSubject<CurrencyExchangeResultModel> mExchangeResult;

    public ExchangeRatesInteractor(@NonNull ICurrencyRatesRepository repository,
                                   @NonNull ExchangeCalculator calculator,
                                   @NonNull ConnectionAvailableReceiver receiver) {
        mRatesRepository = repository;
        mCalculator = calculator;
        mConnAvailableReceiver = receiver;
    }

    @Override
    public Observable<CurrencyExchangeResultModel> exchangeCurrency(@NonNull CurrencyExchangeParamsModel exchangeModel) {
        mExchangeParams = exchangeModel;
        if (mExchangeResult == null) {
            mExchangeResult = BehaviorSubject.create();

            //Start timer for periodic network requests
            Observable
                    .interval(0, DELAY_SECONDS, TimeUnit.SECONDS)
                    .subscribe(this::getRatesFromServer);

            //Subscribe to connection available events - updates rates once connection becomes available
            mConnAvailableReceiver.registerReceiverAndSubscribe()
                    .subscribe(aBoolean -> getRatesFromServer(0));
        }
        return mExchangeResult.asObservable();
    }

    @Override
    public void onExchangeParamsChanged(@NonNull CurrencyExchangeParamsModel exchangeParams) {
        mExchangeParams = exchangeParams;
        if (mCachedCurrencyRates != null) {
            //Recalculate currency exchange amount when params change
            pushExchangedAmount(mCalculator.calcExchangedAmount(mExchangeParams, mCachedCurrencyRates));
        }
    }

    private void getRatesFromServer(long tick) {
        mRatesRepository.getDefaultCurrencyRates()
                .toObservable()
                .filter(this::areRatesUpdated)
                .subscribeOn(Schedulers.io())
                .zipWith(Observable.just(mExchangeParams), this::updateRatesAndCalcAmount)
                .subscribe(this::pushExchangedAmount, throwable -> {
                    //Consume all errors
                    //TODO - Process errors and notify user about some of them
                    });
    }

    private boolean areRatesUpdated(@NonNull CurrencyRatesModel newRates) {
        return mCachedCurrencyRates == null ||
                newRates.getLastUpdatedAt().after(mCachedCurrencyRates.getLastUpdatedAt());
    }

    private CurrencyExchangeResultModel updateRatesAndCalcAmount(@NonNull CurrencyRatesModel rates,
                                                                 @NonNull CurrencyExchangeParamsModel exchangeParams) {
        mCachedCurrencyRates = rates;
        return mCalculator.calcExchangedAmount(exchangeParams, rates);
    }

    private void pushExchangedAmount(@NonNull CurrencyExchangeResultModel resultModel) {
        mExchangeResult.onNext(resultModel);
    }
}