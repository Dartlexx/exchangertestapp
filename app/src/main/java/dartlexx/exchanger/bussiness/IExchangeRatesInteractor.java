package dartlexx.exchanger.bussiness;

import android.support.annotation.NonNull;

import dartlexx.exchanger.bussiness.models.CurrencyExchangeResultModel;
import dartlexx.exchanger.ui.models.CurrencyExchangeParamsModel;
import rx.Observable;

public interface IExchangeRatesInteractor {

    Observable<CurrencyExchangeResultModel> exchangeCurrency(@NonNull CurrencyExchangeParamsModel exchangeModel);

    void onExchangeParamsChanged(@NonNull CurrencyExchangeParamsModel exchangeModel);
}
