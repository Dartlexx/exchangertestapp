package dartlexx.exchanger.ui.models;

import android.support.annotation.NonNull;

import java.math.BigDecimal;

import dartlexx.exchanger.data.repository.CurrencyType;

public class CurrencyExchangeParamsModel {

    private CurrencyType mFromCurrency;
    private CurrencyType mTargetCurrency;
    private BigDecimal mAmount;

    public CurrencyExchangeParamsModel(@NonNull CurrencyType from,
                                       @NonNull CurrencyType target,
                                       @NonNull BigDecimal amount) {
        mFromCurrency = from;
        mTargetCurrency = target;
        mAmount = amount;
    }

    @NonNull
    public CurrencyType getFromCurrency() {
        return mFromCurrency;
    }

    @NonNull
    public CurrencyType getTargetCurrency() {
        return mTargetCurrency;
    }

    @NonNull
    public BigDecimal getAmount() {
        return mAmount;
    }

    @Override
    public String toString() {
        return "[Convert " + mAmount + " " + mFromCurrency + " to '" + mTargetCurrency + "']";
    }
}
