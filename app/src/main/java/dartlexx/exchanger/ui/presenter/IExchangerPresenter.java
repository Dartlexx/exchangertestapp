package dartlexx.exchanger.ui.presenter;

import android.support.annotation.NonNull;

import dartlexx.exchanger.ui.views.IExchangerView;

public interface IExchangerPresenter {

    void bindView(@NonNull IExchangerView view);

    void unbindView();

    void onTopCurrencySelected(int currencyIndex);

    void onBottomCurrencySelected(int currencyIndex);

    void topAmountInputFinished(String amount);

    void bottomAmountInputFinished(String amount);
}
