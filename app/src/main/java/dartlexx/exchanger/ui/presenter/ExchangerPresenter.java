package dartlexx.exchanger.ui.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.math.BigDecimal;
import java.math.RoundingMode;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.bussiness.IExchangeRatesInteractor;
import dartlexx.exchanger.bussiness.models.CurrencyExchangeResultModel;
import dartlexx.exchanger.data.repository.CurrencyType;
import dartlexx.exchanger.ui.models.CurrencyExchangeParamsModel;
import dartlexx.exchanger.ui.views.IExchangerView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class ExchangerPresenter implements IExchangerPresenter {

	private static final int DECIMAL_RATE_SCALE_FOR_STRING = 5;
	private static final int DECIMAL_AMOUNT_SCALE_FOR_STRING = 2;
	private static final RoundingMode DECIMAL_AMOUNT_ROUND_MODE = ExchangerApp.MATH_CONTEXT.getRoundingMode();

	private final IExchangeRatesInteractor mInteractor;
	private final CurrencyType[] mSupportedCurrencies;

	private IExchangerView mView;
	private boolean mIsConvertTopToBottom;
	private BigDecimal mFromAmount;
	private CurrencyType mFromCurrency;
	private CurrencyType mTargetCurrency;
	private Subscription mSubscription;

	public ExchangerPresenter(@NonNull IExchangeRatesInteractor interactor,
							  @NonNull CurrencyType[] supportedCurrencies) {
		if (supportedCurrencies.length < 3) {
			throw new IllegalArgumentException("There should be at least 3 supported currency type");
		}

		mInteractor = interactor;
		mSupportedCurrencies = supportedCurrencies;

		//Set initial exchange params
		mIsConvertTopToBottom = true;
		mFromAmount = BigDecimal.ZERO;
		mFromCurrency = supportedCurrencies[0];
		mTargetCurrency = supportedCurrencies[0];
	}

	@Override
	public void bindView(@NonNull IExchangerView view) {
		mView = view;

		//Subscribe to exchange calculations
		mSubscription = mInteractor
				.exchangeCurrency(new CurrencyExchangeParamsModel(mFromCurrency, mTargetCurrency, mFromAmount))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this::onExchangeResultReceived);
	}

	@Override
	public void unbindView() {
		mSubscription.unsubscribe();
		mView = null;
	}

	@Override
	public void onTopCurrencySelected(int currencyIndex) {
		if (mIsConvertTopToBottom) {
			mFromCurrency = mSupportedCurrencies[currencyIndex];
		} else {
			mTargetCurrency = mSupportedCurrencies[currencyIndex];
		}
		updateExchangedAmountAndRate();
	}

	@Override
	public void onBottomCurrencySelected(int currencyIndex) {
		if (mIsConvertTopToBottom) {
			mTargetCurrency = mSupportedCurrencies[currencyIndex];
		} else {
			mFromCurrency = mSupportedCurrencies[currencyIndex];
		}
		updateExchangedAmountAndRate();
	}

	@Override
	public void topAmountInputFinished(String amount) {
		mFromAmount = new BigDecimal(amount, ExchangerApp.MATH_CONTEXT);
		if (!mIsConvertTopToBottom) {
			onExchangeDirectionChanged();
		}
		updateExchangedAmountAndRate();
	}

	@Override
	public void bottomAmountInputFinished(String amount) {
		mFromAmount = new BigDecimal(amount, ExchangerApp.MATH_CONTEXT);
		if (mIsConvertTopToBottom) {
			onExchangeDirectionChanged();
		}
		updateExchangedAmountAndRate();
	}

	private void onExchangeDirectionChanged() {
		CurrencyType from = mFromCurrency;
		mFromCurrency = mTargetCurrency;
		mTargetCurrency = from;
		mIsConvertTopToBottom = !mIsConvertTopToBottom;
	}

	private void updateExchangedAmountAndRate() {
		CurrencyExchangeParamsModel params = new CurrencyExchangeParamsModel(mFromCurrency, mTargetCurrency,
				mFromAmount);
		mInteractor.onExchangeParamsChanged(params);
	}

    private void onExchangeResultReceived(@NonNull CurrencyExchangeResultModel result) {
		if (mView != null) {
			setExchangeRateString(mView, result.getExchangeRate());
			setTargetAmountText(mView, result.getTargetAmount());
		}
	}

    @VisibleForTesting
	void setExchangeRateString(@NonNull IExchangerView view,
                               @NonNull BigDecimal exchangeRate) {
        if (mIsConvertTopToBottom) {
            String rate = exchangeRate.setScale(DECIMAL_RATE_SCALE_FOR_STRING, DECIMAL_AMOUNT_ROUND_MODE).toString();
            view.setExchangeRateText(CurrencyType.getCurrencySymbol(mFromCurrency),
                    rate,
                    CurrencyType.getCurrencySymbol(mTargetCurrency));
        } else {
			//Invert rate value when converting from bottom to top
            String rate = BigDecimal
                    .ONE.divide(exchangeRate,
                            DECIMAL_RATE_SCALE_FOR_STRING,
                            DECIMAL_AMOUNT_ROUND_MODE)
                    .toString();
            view.setExchangeRateText(CurrencyType.getCurrencySymbol(mTargetCurrency),
                    rate,
                    CurrencyType.getCurrencySymbol(mFromCurrency));
        }
	}

	@VisibleForTesting
	void setTargetAmountText(@NonNull IExchangerView view,
                             @NonNull BigDecimal amount) {
		String amountText = amount.setScale(DECIMAL_AMOUNT_SCALE_FOR_STRING, DECIMAL_AMOUNT_ROUND_MODE).toString();
		if (mIsConvertTopToBottom) {
            view.setBottomAmountText(amountText);
		} else {
            view.setTopAmountText(amountText);
		}
	}
}
