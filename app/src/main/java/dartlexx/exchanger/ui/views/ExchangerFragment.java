package dartlexx.exchanger.ui.views;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import javax.inject.Inject;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.R;
import dartlexx.exchanger.di.exchange.ExchangeModule;
import dartlexx.exchanger.ui.presenter.IExchangerPresenter;

public class ExchangerFragment extends Fragment implements IExchangerView,
		SeekBar.OnSeekBarChangeListener,
		EditText.OnEditorActionListener {

	@Inject
	IExchangerPresenter mPresenter;

	private EditText mTopAmountText;
	private EditText mBottomAmountText;
	private TextView mExchangeRateText;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		ExchangerApp
				.getAppComponent(getContext())
				.addComponent(new ExchangeModule(getContext().getApplicationContext()))
				.inject(this);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		final Activity activity = getActivity();
		mTopAmountText = (EditText) activity.findViewById(R.id.topAmountText);
		mBottomAmountText = (EditText) activity.findViewById(R.id.bottomAmountText);
		mExchangeRateText = (TextView) activity.findViewById(R.id.exchangeRate);

		((SeekBar) activity.findViewById(R.id.topCurrencySelector)).setOnSeekBarChangeListener(this);
		((SeekBar) activity.findViewById(R.id.bottomCurrencySelector)).setOnSeekBarChangeListener(this);

		mTopAmountText.setOnEditorActionListener(this);
		mBottomAmountText.setOnEditorActionListener(this);

		mPresenter.bindView(this);
	}

	@Override
	public void onDestroyView() {
		mPresenter.unbindView();
		super.onDestroyView();
	}

    @Override
    public void setTopAmountText(String amountText) {
        mTopAmountText.setText(amountText);
    }

    @Override
    public void setBottomAmountText(String amountText) {
        mBottomAmountText.setText(amountText);
    }

    @Override
    public void setExchangeRateText(char fromCurrencySymbol,
                                    String exchangeRate,
                                    char targetCurrencySymbol) {
        String rateText = getString(R.string.exchange_rate,
                fromCurrencySymbol,
                exchangeRate,
                targetCurrencySymbol);
        mExchangeRateText.setText(rateText);
    }

	@Override
	public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_DONE ||
                actionId == EditorInfo.IME_ACTION_SEARCH ||
                (event != null &&
                        event.getAction() == KeyEvent.ACTION_DOWN &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

            String amountText = view.getText().toString();
            if (!TextUtils.isEmpty(amountText)) {
                if (view.getId() == R.id.topAmountText) {
                    mPresenter.topAmountInputFinished(amountText);
                } else if (view.getId() == R.id.bottomAmountText) {
                    mPresenter.bottomAmountInputFinished(amountText);
                }
            }
		}
		return false;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if (fromUser) {
			if (seekBar.getId() == R.id.topCurrencySelector) {
				mPresenter.onTopCurrencySelected(progress);
			} else if (seekBar.getId() == R.id.bottomCurrencySelector) {
				mPresenter.onBottomCurrencySelected(progress);
			}
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
        //Do nothing
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
        //Do nothing
	}
}