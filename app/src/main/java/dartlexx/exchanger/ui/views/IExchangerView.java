package dartlexx.exchanger.ui.views;

public interface IExchangerView {

    void setTopAmountText(String amountText);

    void setBottomAmountText(String amountText);

    void setExchangeRateText(char fromCurrencySymbol,
                             String exchangeRate,
                             char targetCurrencySymbol);
}
