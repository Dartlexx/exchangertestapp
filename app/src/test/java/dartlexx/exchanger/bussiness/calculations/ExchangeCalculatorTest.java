package dartlexx.exchanger.bussiness.calculations;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.EnumMap;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.data.repository.CurrencyRatesModel;
import dartlexx.exchanger.data.repository.CurrencyType;

public class ExchangeCalculatorTest {

    private static final String EUR_to_GBP = "1.55694";
    private static final String EUR_to_USD = "0.87754";

    private ExchangeCalculator mCalculator = new ExchangeCalculator();

    @Test
    public void getRateForExchange_SameCurrencies() throws Exception {
        CurrencyRatesModel ratesModel = generateCurrencyRatesModel();

        BigDecimal exchangeRate = mCalculator.getRateForExchange(CurrencyType.EUR,
                CurrencyType.EUR, ratesModel);
        Assert.assertEquals(0, BigDecimal.ONE.compareTo(exchangeRate));

        exchangeRate = mCalculator.getRateForExchange(CurrencyType.GBP,
                CurrencyType.GBP, ratesModel);
        Assert.assertEquals(0, BigDecimal.ONE.compareTo(exchangeRate));

        exchangeRate = mCalculator.getRateForExchange(CurrencyType.USD,
                CurrencyType.USD, ratesModel);
        Assert.assertEquals(0, BigDecimal.ONE.compareTo(exchangeRate));
    }

    @Test
    public void getRateForExchange_FromBase() throws Exception {
        CurrencyRatesModel ratesModel = generateCurrencyRatesModel();

        BigDecimal exchangeRate = mCalculator.getRateForExchange(CurrencyType.EUR,
                CurrencyType.USD, ratesModel);
        Assert.assertEquals(0, new BigDecimal(EUR_to_USD).compareTo(exchangeRate));

        exchangeRate = mCalculator.getRateForExchange(CurrencyType.EUR,
                CurrencyType.GBP, ratesModel);
        Assert.assertEquals(0, new BigDecimal(EUR_to_GBP).compareTo(exchangeRate));
    }

    @Test
    public void getRateForExchange_ToBase() throws Exception {
        CurrencyRatesModel ratesModel = generateCurrencyRatesModel();

        BigDecimal exchangeRate = mCalculator.getRateForExchange(CurrencyType.USD,
                CurrencyType.EUR, ratesModel);
        Assert.assertEquals(0, BigDecimal
                .ONE
                .divide(new BigDecimal(EUR_to_USD), ExchangerApp.MATH_CONTEXT)
                .compareTo(exchangeRate));

        exchangeRate = mCalculator.getRateForExchange(CurrencyType.GBP,
                CurrencyType.EUR, ratesModel);
        Assert.assertEquals(0, BigDecimal
                .ONE
                .divide(new BigDecimal(EUR_to_GBP), ExchangerApp.MATH_CONTEXT)
                .compareTo(exchangeRate));
    }

    @Test
    public void getRateForExchange_WithoutBase() throws Exception {
        CurrencyRatesModel ratesModel = generateCurrencyRatesModel();

        BigDecimal gbp_usd = mCalculator.getRateForExchange(CurrencyType.GBP,
                CurrencyType.USD, ratesModel);
        Assert.assertEquals(0, new BigDecimal(EUR_to_USD, ExchangerApp.MATH_CONTEXT)
                .divide(new BigDecimal(EUR_to_GBP), ExchangerApp.MATH_CONTEXT)
                .compareTo(gbp_usd));

        BigDecimal usd_gbp = mCalculator.getRateForExchange(CurrencyType.USD,
                CurrencyType.GBP, ratesModel);
        Assert.assertEquals(0, new BigDecimal(EUR_to_GBP, ExchangerApp.MATH_CONTEXT)
                .divide(new BigDecimal(EUR_to_USD), ExchangerApp.MATH_CONTEXT)
                .compareTo(usd_gbp));

        Assert.assertEquals(0, BigDecimal
                .ONE
                .divide(gbp_usd, ExchangerApp.MATH_CONTEXT)
                .compareTo(usd_gbp));
    }

    
    @Test
    public void findRateForBaseCurrency_NoCurrencyRateError() throws Exception {
        EnumMap<CurrencyType, BigDecimal> rates = new EnumMap<>(CurrencyType.class);
        rates.put(CurrencyType.GBP, BigDecimal.TEN);
        CurrencyRatesModel ratesModel = new CurrencyRatesModel(CurrencyType.EUR, new Date(), rates);

        try {
            mCalculator.findRateForBaseCurrency(CurrencyType.USD, ratesModel);
        } catch (Throwable exc) {
            Assert.assertTrue(exc instanceof ExchangeCalculationException);
        }
    }

    @Test
    public void findRateForBaseCurrency_IncorrectRateError() throws Exception {
        EnumMap<CurrencyType, BigDecimal> rates = new EnumMap<>(CurrencyType.class);
        rates.put(CurrencyType.GBP, new BigDecimal("-5.333"));
        rates.put(CurrencyType.USD, new BigDecimal(0));
        CurrencyRatesModel ratesModel = new CurrencyRatesModel(CurrencyType.EUR, new Date(), rates);

        try {
            mCalculator.findRateForBaseCurrency(CurrencyType.USD, ratesModel);
        } catch (Throwable exc) {
            Assert.assertTrue(exc instanceof ExchangeCalculationException);
        }

        try {
            mCalculator.findRateForBaseCurrency(CurrencyType.GBP, ratesModel);
        } catch (Throwable exc) {
            Assert.assertTrue(exc instanceof ExchangeCalculationException);
        }
    }

    @Test
    public void findRateForBaseCurrency_Success() throws Exception {
        CurrencyRatesModel ratesModel = generateCurrencyRatesModel();

        BigDecimal rateUsd = mCalculator.findRateForBaseCurrency(CurrencyType.USD, ratesModel);
        Assert.assertEquals(0, new BigDecimal(EUR_to_USD).compareTo(rateUsd));

        BigDecimal rateGbp = mCalculator.findRateForBaseCurrency(CurrencyType.GBP, ratesModel);
        Assert.assertEquals(0, new BigDecimal(EUR_to_GBP).compareTo(rateGbp));
    }

    private CurrencyRatesModel generateCurrencyRatesModel() {
        EnumMap<CurrencyType, BigDecimal> rates = new EnumMap<>(CurrencyType.class);
        rates.put(CurrencyType.GBP, new BigDecimal(EUR_to_GBP));
        rates.put(CurrencyType.USD, new BigDecimal(EUR_to_USD));
        return new CurrencyRatesModel(CurrencyType.EUR, new Date(), rates);
    }
}