package dartlexx.exchanger.data.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import dartlexx.exchanger.data.network.FixerIoRatesModel;
import dartlexx.exchanger.data.network.ICurrencyRatesRequest;
import rx.Single;
import rx.observers.TestSubscriber;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyRatesRepositoryTest {

    @Mock
    FixerIoRatesModel mResult;

    @Mock
    CurrencyRatesModel mRates;

    @Mock
    ICurrencyRatesRequest mNetworkRequest;

    private CurrencyRatesRepository mRepository;

    @Before
    public void setUp() throws Exception {
        Single<FixerIoRatesModel> mSingle = Single.just(mResult);

        Mockito.when(mNetworkRequest.getRates()).thenReturn(mSingle);
        Mockito.when(mNetworkRequest.getRates(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(mSingle);
        Mockito.when(mResult.convertToCurrencyRates()).thenReturn(mRates);

        mRepository = new CurrencyRatesRepository(mNetworkRequest);
    }

    @Test
    public void getDefaultCurrencyRates_Success() throws Exception {
        TestSubscriber<CurrencyRatesModel> testSubscriber = TestSubscriber.create();
        mRepository.getDefaultCurrencyRates().subscribe(testSubscriber);

        Mockito.verify(mNetworkRequest, Mockito.atLeastOnce()).getRates();
        Mockito.verify(mResult, Mockito.atLeastOnce()).convertToCurrencyRates();
        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        //Test got correct result as expected
        Assert.assertEquals(mRates, testSubscriber.getOnNextEvents().get(0));
    }

    @Test
    public void getDefaultCurrencyRates_Error() throws Exception {
        Mockito.when(mResult.convertToCurrencyRates()).thenThrow(new ModelMappingException(""));
        TestSubscriber<CurrencyRatesModel> testSubscriber = TestSubscriber.create();
        mRepository.getDefaultCurrencyRates().subscribe(testSubscriber);

        Mockito.verify(mNetworkRequest, Mockito.atLeastOnce()).getRates();
        testSubscriber.awaitTerminalEvent();
        //Test got expected error
        testSubscriber.assertError(ModelMappingException.class);
    }

    @Test
    public void getCurrencyRatesFor() throws Exception {
        CurrencyType[] rates = {CurrencyType.GBP, CurrencyType.USD};
        TestSubscriber<CurrencyRatesModel> testSubscriber = TestSubscriber.create();
        mRepository.getCurrencyRatesFor(CurrencyType.EUR, rates).subscribe(testSubscriber);

        Mockito.verify(mNetworkRequest, Mockito.atLeastOnce()).getRates("EUR", "GBP,USD");
        Mockito.verify(mResult, Mockito.atLeastOnce()).convertToCurrencyRates();
        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        //Test got correct result as expected
        Assert.assertEquals(mRates, testSubscriber.getOnNextEvents().get(0));
    }

    @Test
    public void buildCurrenciesEnumeration() throws Exception {
        CurrencyType[] rates = {
                CurrencyType.GBP,
                CurrencyType.USD,
                CurrencyType.USD,
                CurrencyType.EUR,
                CurrencyType.GBP,
                CurrencyType.EUR,
        };
        Assert.assertEquals("GBP,USD,USD,EUR,GBP,EUR",
                CurrencyRatesRepository.buildCurrenciesEnumeration(rates));
    }
}