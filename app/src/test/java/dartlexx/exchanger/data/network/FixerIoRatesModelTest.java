package dartlexx.exchanger.data.network;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.data.repository.CurrencyRatesModel;
import dartlexx.exchanger.data.repository.CurrencyType;
import dartlexx.exchanger.data.repository.ModelMappingException;

public class FixerIoRatesModelTest {

    private static final String EUR_to_GBP = "0.8";
    private static final String EUR_to_USD = "1.2";

    private String mBase;
    private String mDate;
    private Map<String, String> mRates;

    @Before
    public void setUp() throws Exception {
        mBase = CurrencyType.EUR.toString();
        mDate = "2017-07-04";
        mRates = new HashMap<>(2);
        mRates.put(CurrencyType.USD.toString(), EUR_to_USD);
        mRates.put(CurrencyType.GBP.toString(), EUR_to_GBP);
    }

    @Test
    public void emptyBaseCurrency() throws Exception {
        checkReceivedError("", mDate, mRates);
    }

    @Test
    public void unknownBaseCurrency() throws Exception {
        checkReceivedError("UNKNOWN", mDate, mRates);
    }

    @Test
    public void emptyDate() throws Exception {
        checkReceivedError(mBase, "", mRates);
    }

    @Test
    public void wrongFormatOfDate() throws Exception {
        checkReceivedError(mBase, "UNKNOWN", mRates);
        checkReceivedError(mBase, "05.12.2017", mRates);
        checkReceivedError(mBase, "2017/05/04", mRates);
    }

    @Test
    public void nullRates() throws Exception {
        checkReceivedError(mBase, mDate, null);
    }

    @Test
    public void emptyRates() throws Exception {
        checkReceivedError(mBase, mDate, Collections.emptyMap());
    }

    @Test
    public void incorrectRates() throws Exception {
        Map<String, String> testMap = new HashMap<>(5);
        testMap.put("FIRST", "1.5");
        checkReceivedError(mBase, mDate, testMap);

        testMap.remove("FIRST");
        testMap.put("USD", "Hello world");
        checkReceivedError(mBase, mDate, testMap);

        testMap.put("USD", "");
        checkReceivedError(mBase, mDate, testMap);
    }

    @Test
    public void checkSuccess() throws Exception {
        FixerIoRatesModel fromModel = new FixerIoRatesModel(mBase, mDate, mRates);
        CurrencyRatesModel toModel = fromModel.convertToCurrencyRates();

        Assert.assertEquals(CurrencyType.EUR, toModel.getBaseCurrency());
        Assert.assertEquals(2, toModel.getCurrencyRates().size());
        Assert.assertEquals(new BigDecimal(EUR_to_GBP, ExchangerApp.MATH_CONTEXT),
                toModel.getCurrencyRates().get(CurrencyType.GBP));
        Assert.assertEquals(new BigDecimal(EUR_to_USD, ExchangerApp.MATH_CONTEXT),
                toModel.getCurrencyRates().get(CurrencyType.USD));
    }

    private void checkReceivedError(String base, String date, Map<String, String> rates) {
        FixerIoRatesModel model = new FixerIoRatesModel(base, date, rates);
        try {
            model.convertToCurrencyRates();
            Assert.fail("Should drop with exception");
        } catch (Throwable exc) {
            Assert.assertTrue("Exception class is " + exc.getClass().getSimpleName(),
                    exc instanceof ModelMappingException);
        }
    }
}