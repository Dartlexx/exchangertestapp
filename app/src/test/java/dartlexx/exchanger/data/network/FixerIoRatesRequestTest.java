package dartlexx.exchanger.data.network;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.data.repository.ModelMappingException;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.observers.TestSubscriber;

@RunWith(MockitoJUnitRunner.class)
public class FixerIoRatesRequestTest {

    @Mock
    IFixerIoExchangeRates mRatesInterface;

    @Mock
    Call<FixerIoRatesModel> mNetworkCall;

    @Mock
    FixerIoRatesModel mResult;

    private FixerIoRatesRequest mRequest;

    @Before
    public void setUp() throws Exception {
        mRequest = new FixerIoRatesRequest(mRatesInterface,
                ExchangerApp.DEFAULT_BASE_CURRENCY,
                ExchangerApp.DEFAULT_EXCHANGE_RATES);

        Mockito.when(mRatesInterface.ratesForBaseCurrency(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(mNetworkCall);
    }

    @Test
    public void getDefaultRates_NetworkCallCreated() throws Exception {
        mRequest.getRates();
        Mockito.verify(mRatesInterface, Mockito.atLeastOnce())
                .ratesForBaseCurrency(ExchangerApp.DEFAULT_BASE_CURRENCY,
                        ExchangerApp.DEFAULT_EXCHANGE_RATES);
        Mockito.verify(mNetworkCall, Mockito.never()).clone();
    }

    @Test
    public void getDefaultRates_NetworkCallClonedOnSubsequentCalls() throws Exception {
        Mockito.when(mNetworkCall.clone()).thenReturn(mNetworkCall);
        mRequest.getRates();
        mRequest.getRates();
        Mockito.verify(mNetworkCall, Mockito.atLeastOnce()).clone();
    }

    @Test
    public void getDefaultRates_SuccessfulCall() throws Exception {
        Mockito.when(mNetworkCall.execute()).thenReturn(Response.success(mResult));
        TestSubscriber<FixerIoRatesModel> testSubscriber = TestSubscriber.create();
        mRequest.getRates().subscribe(testSubscriber);

        Mockito.verify(mNetworkCall, Mockito.atLeastOnce()).execute();
        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        //Test got correct result as expected
        Assert.assertEquals(mResult, testSubscriber.getOnNextEvents().get(0));
    }

    @Test
    public void getDefaultRates_FailedCall_Exception() throws Exception {
        Mockito.when(mNetworkCall.execute()).thenThrow(new IOException("No connection"));
        TestSubscriber<FixerIoRatesModel> testSubscriber = TestSubscriber.create();
        mRequest.getRates().subscribe(testSubscriber);

        Mockito.verify(mNetworkCall, Mockito.atLeastOnce()).execute();
        testSubscriber.awaitTerminalEvent();
        //Test got expected error
        testSubscriber.assertError(IOException.class);
    }

    @Test
    public void getDefaultRates_FailedCall_ErrorResponse() throws Exception {
        Mockito.when(mNetworkCall.execute()).thenReturn(Response.error(403,
                ResponseBody.create(MediaType.parse("application/json"),
                        "{\"error\": \"Bad URL\"}")));
        TestSubscriber<FixerIoRatesModel> testSubscriber = TestSubscriber.create();
        mRequest.getRates().subscribe(testSubscriber);

        Mockito.verify(mNetworkCall, Mockito.atLeastOnce()).execute();
        testSubscriber.awaitTerminalEvent();
        //Test got expected error
        testSubscriber.assertError(ModelMappingException.class);
    }

    @Test
    public void getRatesForOtherCurrency_SuccessfulCall() throws Exception {
        Mockito.when(mNetworkCall.execute()).thenReturn(Response.success(mResult));
        TestSubscriber<FixerIoRatesModel> testSubscriber = TestSubscriber.create();
        mRequest.getRates("CURR1", "LIST").subscribe(testSubscriber);

        Mockito.verify(mRatesInterface, Mockito.atLeastOnce())
                .ratesForBaseCurrency("CURR1", "LIST");
        Mockito.verify(mNetworkCall, Mockito.atLeastOnce()).execute();
        testSubscriber.assertTerminalEvent();
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
        //Test got correct result as expected
        Assert.assertEquals(mResult, testSubscriber.getOnNextEvents().get(0));
    }
}