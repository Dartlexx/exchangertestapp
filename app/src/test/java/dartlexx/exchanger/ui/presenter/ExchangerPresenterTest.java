package dartlexx.exchanger.ui.presenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import dartlexx.exchanger.ExchangerApp;
import dartlexx.exchanger.bussiness.IExchangeRatesInteractor;
import dartlexx.exchanger.data.repository.CurrencyType;
import dartlexx.exchanger.ui.models.CurrencyExchangeParamsModel;
import dartlexx.exchanger.ui.views.IExchangerView;

@RunWith(MockitoJUnitRunner.class)
public class ExchangerPresenterTest {

    @Mock
    IExchangeRatesInteractor mInteractor;

    @Mock
    IExchangerView mView;

    private ExchangerPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        mPresenter = new ExchangerPresenter(mInteractor, ExchangerApp.SUPPORTED_CURRENCIES);
    }

    @Test
    public void onTopCurrencySelected() throws Exception {
        mPresenter.onTopCurrencySelected(1);
        ArgumentCaptor<CurrencyExchangeParamsModel> argCaptor =
                ArgumentCaptor.forClass(CurrencyExchangeParamsModel.class);
        Mockito.verify(mInteractor, Mockito.atLeastOnce())
                .onExchangeParamsChanged(argCaptor.capture());
        // Check correct FROM currency was sent
        Assert.assertEquals(ExchangerApp.SUPPORTED_CURRENCIES[1],
                argCaptor.getValue().getFromCurrency());

        // Switch exchange direction
        mPresenter.bottomAmountInputFinished("12");
        Mockito.reset(mInteractor);

        mPresenter.onTopCurrencySelected(0);
        Mockito.verify(mInteractor, Mockito.atLeastOnce())
                .onExchangeParamsChanged(argCaptor.capture());
        // Check correct TARGET currency was sent
        Assert.assertEquals(ExchangerApp.SUPPORTED_CURRENCIES[0],
                argCaptor.getValue().getTargetCurrency());
    }

    @Test
    public void onBottomCurrencySelected() throws Exception {
        mPresenter.onBottomCurrencySelected(2);
        ArgumentCaptor<CurrencyExchangeParamsModel> argCaptor =
                ArgumentCaptor.forClass(CurrencyExchangeParamsModel.class);
        Mockito.verify(mInteractor, Mockito.atLeastOnce())
                .onExchangeParamsChanged(argCaptor.capture());
        // Check correct TARGET currency was sent
        Assert.assertEquals(ExchangerApp.SUPPORTED_CURRENCIES[2],
                argCaptor.getValue().getTargetCurrency());

        // Switch exchange direction
        mPresenter.bottomAmountInputFinished("75");
        Mockito.reset(mInteractor);

        mPresenter.onBottomCurrencySelected(1);
        Mockito.verify(mInteractor, Mockito.atLeastOnce())
                .onExchangeParamsChanged(argCaptor.capture());
        // Check correct FROM currency was sent
        Assert.assertEquals(ExchangerApp.SUPPORTED_CURRENCIES[1],
                argCaptor.getValue().getFromCurrency());
    }

    @Test
    public void topAmountInputFinished() throws Exception {
        mPresenter.topAmountInputFinished("555.23");
        ArgumentCaptor<CurrencyExchangeParamsModel> argCaptor =
                ArgumentCaptor.forClass(CurrencyExchangeParamsModel.class);
        Mockito.verify(mInteractor, Mockito.atLeastOnce())
                .onExchangeParamsChanged(argCaptor.capture());

        // Check correct currency amount was sent
        Assert.assertEquals(0,
                new BigDecimal("555.23", ExchangerApp.MATH_CONTEXT)
                        .compareTo(argCaptor.getValue().getAmount()));
    }

    @Test
    public void bottomAmountInputFinished() throws Exception {
        mPresenter.bottomAmountInputFinished("51.13");
        ArgumentCaptor<CurrencyExchangeParamsModel> argCaptor =
                ArgumentCaptor.forClass(CurrencyExchangeParamsModel.class);
        Mockito.verify(mInteractor, Mockito.atLeastOnce())
                .onExchangeParamsChanged(argCaptor.capture());

        // Check correct currency amount was sent
        Assert.assertEquals(0,
                new BigDecimal("51.13", ExchangerApp.MATH_CONTEXT)
                        .compareTo(argCaptor.getValue().getAmount()));
    }

    @Test
    public void setExchangeRateString() throws Exception {
        mPresenter.setExchangeRateString(mView, new BigDecimal("2.0"));
        Mockito.verify(mView, Mockito.atLeastOnce()).setExchangeRateText(
                CurrencyType.getCurrencySymbol(ExchangerApp.SUPPORTED_CURRENCIES[0]),
                "2.00000",
                CurrencyType.getCurrencySymbol(ExchangerApp.SUPPORTED_CURRENCIES[0]));

        // Switch direction
        mPresenter.bottomAmountInputFinished("12");
        Mockito.reset(mView);
        mPresenter.setExchangeRateString(mView, new BigDecimal("2.0"));
        Mockito.verify(mView, Mockito.atLeastOnce()).setExchangeRateText(
                CurrencyType.getCurrencySymbol(ExchangerApp.SUPPORTED_CURRENCIES[0]),
                "0.50000",
                CurrencyType.getCurrencySymbol(ExchangerApp.SUPPORTED_CURRENCIES[0]));

        // Change currencies
        mPresenter.onTopCurrencySelected(2);
        mPresenter.onBottomCurrencySelected(1);
        Mockito.reset(mView);
        mPresenter.setExchangeRateString(mView, new BigDecimal("2.0"));
        Mockito.verify(mView, Mockito.atLeastOnce()).setExchangeRateText(
                CurrencyType.getCurrencySymbol(ExchangerApp.SUPPORTED_CURRENCIES[2]),
                "0.50000",
                CurrencyType.getCurrencySymbol(ExchangerApp.SUPPORTED_CURRENCIES[1]));
    }

    @Test
    public void setTargetAmountText() throws Exception {
        mPresenter.setTargetAmountText(mView, new BigDecimal("2.0033333"));
        Mockito.verify(mView, Mockito.atLeastOnce()).setBottomAmountText("2.00");

        // Switch direction
        mPresenter.bottomAmountInputFinished("12");
        Mockito.reset(mView);

        mPresenter.setTargetAmountText(mView, new BigDecimal("2.001546"));
        Mockito.verify(mView, Mockito.atLeastOnce()).setTopAmountText("2.00");
    }
}