**Comments**:

* While implementing this test application I've tried to get acquainted with Clean Architecture. And I'd say it's a great architecture blueprint for any application. It's super easy to test and build a demo version at any point of development.
* I've also tried to use RxJava that I'm learning atm. Please, don't judge me too harsh, I'm still trying to understand it's ideology and technics :-)
* I've tried to make app sustainable to change of currencies list or currency rate provider service, or any similar changes for that matter.


**Things not done**:

* I haven't implemented any error proccessing, no errors are shown to user. I think it's not that important for that test, and it's easy to implement with current app design.
* UI design is oversimplified now, but it's easy to improve it or remake from scratch.


**How to build:**
Everything is fairly simple, except that ***JAVA_HOME*** environment variable need to be set in "app\build.gradle". It's used by RetroLambda plugin.